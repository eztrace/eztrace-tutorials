# Analyzing an OpenMP application with EZTrace

*This tutorial is meant for EZTrace version 2 or higher. If you're using EZTrace 1.X, you can either upgrade to EZTrace 2, or explore the [EZTrace 1.x tutorials](https://eztrace.gitlab.io/eztrace/tutorials/).*

## Installing EZTrace

EZTrace provides 2 plugins for analyzing OpenMP applications:
- `ompt` relies on the OMPT interface for intercepting OpenMP events. It only works if the OpenMP runtime provides this interface. For example `clang` does provides ompt, but not `gcc`
- `openmp` relies on a source-to-source compiler (`opari2`) for instrumenting the OpenMP application. It should work on any OpenMP runtime, but it requires an additionnal source code instrumentation.

First, install EZTrace and make sure the `ompt` and/or `openmp` modules are enabled:
```
$ cmake . -DEZTRACE_ENABLE_OMPT=ON -DEZTRACE_ENABLE_OPENMP=ON
[...]

Enabled modules:
  OpenMP module  : ON
  OMPT module    : ON
  [...]

-- Configuring done
-- Generating done
-- Build files have been written to: [...]
$ make -j 16 install
```

Check the installation of EZTrace:
```
$ eztrace_avail
[...]
openmp  "Module for OpenMP"
ompt    "Module for OpenMP directives, using OMPT"
```

## Running an application with EZTrace
Let's analyze this [OpenMP FFT application](fft_openmp.c)[^source_fft]. First, compile it:
```
clang -o fft_openmp fft_openmp.c -fopenmp -lm
```

You can run the application with:
```
./fft_openmp
```

### Using the ompt module

To analyze it with EZTrace `ompt` module, simply add `eztrace -t ompt` to the command line:
```
eztrace -t ompt ./fft_openmp
```

This generates an OTF2 trace named `fft_openmp_trace`.

### Using the openmp module

To analyze the application with the openmp module, you need to recompile it with `eztrace_cc`:
```
eztrace_cc clang -o fft_openmp fft_openmp.c -fopenmp -lm
```

Now, run the application with `eztrace -t openmp`:
```
eztrace -t openmp ./fft_openmp
```

This generates an OTF2 trace named `fft_openmp_trace`.

## Analyzing the execution of the application

### Visualizing an execution trace

You can visualize the execution trace using [ViTE](https://solverstack.gitlabpages.inria.fr/vite/), or other OTF2 visualization programs (eg. Vampir). Make sure ViTE was compiled with support for OTF2 trace format.

To visualize the trace with ViTE, run:
```
vite fft_openmp_trace/eztrace_log.otf2
```

![](fft_openmp_trace.png)

[^source_fft]: The OpenMP-based FFT application is based on [John Burkardt implementation](https://people.sc.fsu.edu/~jburkardt/c_src/fft_openmp/fft_openmp.html).
