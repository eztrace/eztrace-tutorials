# EZTrace tutorials

*This tutorial repository is meant for EZTrace version 2 or higher. If you're using EZTrace 1.X, you can either upgrade to EZTrace 2, or explore the [EZTrace 1.x tutorials](https://eztrace.gitlab.io/eztrace/tutorials/).*



## Documentation

The documentation of EZTrace [can be found here](https://gitlab.com/eztrace/eztrace#Documentation).
- [Building EZTrace](https://gitlab.com/eztrace/eztrace/-/blob/master/doc/building.md)
- [Using EZTrace](https://gitlab.com/eztrace/eztrace/-/blob/master/doc/using.md)
- [EZTrace plugins](https://gitlab.com/eztrace/eztrace/-/blob/master/doc/plugin.md)
- [Frequently asked questions](https://gitlab.com/eztrace/eztrace/-/blob/master/doc/faq.md)


## Tutorials

- [Using EZTrace for MPI applications](mpi/README.md)
- [Using EZTrace for OpenMP applications](openmp/README.md)
- [Using EZTrace for Python applications](python/README.md)
- [Using EZTrace for CUDA applications](cuda/README.md) (TODO)
- [Measuring the energy consumption of an application with EZTrace](rapl/README.md) (TODO)
- [Creating an EZTrace module](plugin/README.md) (TODO)

