#!/bin/bash

epochs=2
nb_ranks=4

EZTRACE=/root/eztrace/install/bin/eztrace

export EZTPY_FUNCTION_FILTER=pytorch_functions.txt

export OMPI_ALLOW_RUN_AS_ROOT=1
export OMPI_ALLOW_RUN_AS_ROOT_CONFIRM=1

mpirun -np $nb_ranks $EZTRACE -m -t "mpi python" pytorch_cifar10_resnet50.py --epochs=$epochs
