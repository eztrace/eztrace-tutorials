# Analyzing a Python application with EZTrace

In this tutorial, we analyze a distributed deep-learning application
with EZTrace.

## Running the application

This application uses Horovod+PyTorch, and relies on MPI (through
Horovod) for communicating between computing nodes. You need to
install a few python packages (eg `torch`, `horovod`, etc.) You can
also use an existing docker container
(eg. `horovod/horovod-cpu:latest`) that ships these packages.

For this tutorial, we don't care about the model accuracy, and we just
want a program that runs fast. Thus, the pytorch script only uses a
subset of the CIFAR-10 dataset, and only trains for 2 epochs.


```
$ docker run  -v $PWD:/root -it horovod/horovod-cpu:latest  sh -c "cd /root ; bash run_pytorch.sh"
Training with  4  mpi processes
[...]
Train Epoch     #1:   0%|          | 0/1 [00:00<?, ?it/s]Training epoch  0  took  29.37992000579834  s

Train Epoch     #1: 100%|██████████| 1/1 [00:29<00:00, 29.38s/it, loss=7.06, accuracy=0]
Train Epoch     #2: 100%|██████████| 1/1 [00:27<00:00, 27.87s/it, loss=3.79, accuracy=14]Training epoch  1  took  27.870156049728394  s

```


## Installing EZTrace
First, install EZTrace and make sure the Python and MPI modules are enabled:
```
$ docker run  -v $PWD:/root -it horovod/horovod-cpu:latest bash
root@2a3458e827a8:/horovod/examples# cd /root/

# Install OTF2

root@2a3458e827a8:~# wget https://perftools.pages.jsc.fz-juelich.de/cicd/otf2/tags/otf2-3.0.3/otf2-3.0.3.tar.gz
root@2a3458e827a8:~# tar xf otf2-3.0.3.tar.gz ; cd otf2-3.0.3 ; ./configure --prefix=$PWD/install &&make install -j 16 ; cd /root
root@2a3458e827a8:~/# PATH=/root/otf2-3.0.3/install/bin/:$PATH

# Install EZTrace
root@2a3458e827a8:~# git clone --branch dev https://gitlab.com/eztrace/eztrace.git
root@2a3458e827a8:~# cd eztrace ; cmake . -DCMAKE_INSTALL_PREFIX=$PWD/install -DEZTRACE_ENABLE_MPI=ON -DEZTRACE_ENABLE_PYTHON=ON
[...]

Enabled modules:
   MPI module               : ON
   Python module            : ON
[...]

-- Configuring done
-- Generating done
-- Build files have been written to: /root/eztrace
root@2a3458e827a8:~eztrace/# make -j 16 install
```

Check the installation of EZTrace:
```
root@2a3458e827a8:~eztrace/# /root/eztrace/install/bin/eztrace_avail
[...]
mpi     "Module for MPI functions"
python  "Module for python functions"

```

## Running a Python application with EZTrace

By default, the Python module for EZTrace logs all the python
functions. You can select the functions to trace by listing the
functions in a file, and passing this file through the
`EZTPY_FUNCTION_FILTER` environment variable:

```
$ cat pytorch_functions.txt
zero_grad
backward
forward
model
loss_fn
$ export EZTPY_FUNCTION_FILTER=pytorch_functions.txt
```

Then, tracing the application works as with regular MPI application, eg:
```
$ mpirun -np 4  eztrace -m -t "mpi python" pytorch_cifar10_resnet50.py --epochs=2
```

You can use `run_eztrace_pytorch.sh` that runs eztrace within the docker container:

```
$ docker run  -v $PWD:/root -it horovod/horovod-cpu:latest  sh -c "cd /root ; bash run_eztrace_pytorch.sh"
+ mpirun -np 4 /root/eztrace/install/bin/eztrace -m -t 'mpi python' pytorch_cifar10_resnet50.py --epochs=2
[P0T0] Starting EZTrace (pid: 11)...
[...]
Training with  4  mpi processes

Files already downloaded and verified
Train Epoch     #1:   0%|          | 0/1 [00:00<?, ?it/s]Training epoch  0  took  25.891034364700317  s

Train Epoch     #1: 100%|██████████| 1/1 [00:25<00:00, 25.89s/it, loss=7.06, accuracy=0]
Train Epoch     #2: 100%|██████████| 1/1 [00:26<00:00, 26.67s/it, loss=3.79, accuracy=14]Training epoch  1  took  26.677727222442627  s


[...]
[P0T0] Stopping EZTrace (pid:11)...
```

This generates an OTF2 trace named [`pytorch_cifar10_resnet50.py_trace`](pytorch_cifar10_resnet50.py_trace.tgz).

## Analyzing the execution of the application

### Visualizing an execution trace

You can visualize the execution trace using [ViTE](https://solverstack.gitlabpages.inria.fr/vite/), or other OTF2 visualization programs (eg. Vampir). Make sure ViTE was compiled with support for OTF2 trace format.

To visualize the trace with ViTE, run:
```
vite pytorch_cifar10_resnet50.py_trace/eztrace_log.otf2
```

![](trace_resnet_full.png)

As you can see, there are a lot of "useless" threads. In ViTE, you can select the threads to display in the `Preferences` -> `Node selection` menu. For each process, select thread \#0 (the thread in charge of MPI communication) and \#1 (the thread in charge of computing).

![](trace_resnet_selected_threads.png)

In this trace, we observe that the communication thread spend their
time performing `MPI_AllReduce` (in Dark Red). The computing threads
spend about 30% of their time doing `forward` pass, and 60% of their
time doing the `backward` pass.
