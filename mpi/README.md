# Analyzing an MPI application with EZTrace

*This tutorial is meant for EZTrace version 2 or higher. If you're using EZTrace 1.X, you can either upgrade to EZTrace 2, or explore the [EZTrace 1.x tutorials](https://eztrace.gitlab.io/eztrace/tutorials/).*

## Installing EZTrace
First, install EZTrace and make sure the MPI module is enabled:
```
$ cmake . -DEZTRACE_ENABLE_MPI=ON
[...]

Enabled modules:
  MPI module     : ON
  [...]

-- Configuring done
-- Generating done
-- Build files have been written to: [...]
$ make -j 16 install
```

Check the installation of EZTrace:
```
$ eztrace_avail
[...]
mpi     Module for MPI functions
```

## Running an MPI application with EZTrace
Let's analyze this [MPI stencil application](stencil_mpi.c). First, compile it:
```
mpicc -o stencil_mpi stencil_mpi.c
```

You can run the application with:
```
mpirun -np 4 ./stencil_mpi 1024
```

To analyze it with EZTrace, simply add `eztrace -t mpi` to the command line:
```
mpirun -np 4 eztrace -t mpi ./stencil_mpi 1024
```

This generates an OTF2 trace named `stencil_mpi_trace`.

## Analyzing the execution of the application

### Visualizing an execution trace

You can visualize the execution trace using [ViTE](https://solverstack.gitlabpages.inria.fr/vite/), or other OTF2 visualization programs (eg. Vampir). Make sure ViTE was compiled with support for OTF2 trace format.

To visualize the trace with ViTE, run:
```
vite stencil_mpi_trace/eztrace_log.otf2
```

![](stencil_mpi_trace.png)


## Running an MPI+OpenMP application with EZTrace

The stencil application mixes MPI and OpenMP. You can analyze both
parallelization with EZTrace by selecting several EZTrace modules.  As
seen in the [OpenMP tutorial](../openmp/README.md), EZTrace provides
two OpenMP modules. Let's use the `openmp` module (but the `ompt`
module would work the same way).

First, let's compile the application:

```
eztrace_cc mpicc -o stencil_mpi stencil_mpi.c -fopenmp
```

To analyze the application with EZTrace, simply add `eztrace -t "mpi openmp"` to the command line:
```
export OMP_NUM_THREADS=4
mpirun -np 4 eztrace -t "mpi openmp" ./stencil_mpi 1024
```

This generates an OTF2 trace named `stencil_mpi_trace`.


![](stencil_mpi_openmp_trace.png)
